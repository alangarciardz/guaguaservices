//scp  -i "/Users/alangarcia/Downloads/Downloadsold/monitoreo.pem"  routes.js ec2-user@ec2-54-89-234-112.compute-1.amazonaws.com:/var/www/html/service/GG/server/config/
const nodemailer = require('nodemailer')
const config = require('./config')
const xoauth2 = require('xoauth2')
var fs = require('fs');

// create reusable transporter object using the default SMTP transport
var transporter = nodemailer.createTransport("SMTP", {
    service: "Gmail",
    auth: {
        type: "OAuth2",
        user: config.mailUser,
        pass: config.mailPass
    }
});

module.exports = function (app) {
    // send mail with defined transport object

    app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });

    app.get('/', function (req, res) {
        console.log("API servicios de transporte GUAGUA");
        res.json({'result': {}});
    });
    
    app.post('/send', function(req, res){  
        
        console.log("Procesado datos para enviar email....")
        // setup e-mail data with unicode symbols
        console.log(req.body.html);
        var mailOptions = {
            from: ''+config.mailUser+'', // sender address
            to: req.body.receivers, // list of receivers
            subject: req.body.subject, // Subject line
            //text: req.body.text, // plaintext body
            //html: '<!doctype html><html> <head> <meta name="viewport" content="width=device-width" /> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <title>Simple Transactional Email</title> <style> /* ------------------------------------- GLOBAL RESETS ------------------------------------- */ img { border: none; -ms-interpolation-mode: bicubic; max-width: 100%; } body { background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; } table { border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; } table td { font-family: sans-serif; font-size: 14px; vertical-align: top; } /* ------------------------------------- BODY & CONTAINER ------------------------------------- */ .body { background-color: #f6f6f6; width: 100%; } /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */ .container { display: block; Margin: 0 auto !important; /* makes it centered */ max-width: 580px; padding: 10px; width: 580px; } /* This should also be a block element, so that it will fill 100% of the .container */ .content { box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px; } /* ------------------------------------- HEADER, FOOTER, MAIN ------------------------------------- */ .main { background: #fff; border-radius: 3px; width: 100%; } .wrapper { box-sizing: border-box; padding: 20px; } .footer { clear: both; padding-top: 10px; text-align: center; width: 100%; } .footer td, .footer p, .footer span, .footer a { color: #999999; font-size: 12px; text-align: center; } /* ------------------------------------- TYPOGRAPHY ------------------------------------- */ h1, h2, h3, h4 { color: #000000; font-family: sans-serif; font-weight: 400; line-height: 1.4; margin: 0; Margin-bottom: 30px; } h1 { font-size: 35px; font-weight: 300; text-align: center; text-transform: capitalize; } p, ul, ol { font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px; } p li, ul li, ol li { list-style-position: inside; margin-left: 5px; } a { color: #3498db; text-decoration: underline; } /* ------------------------------------- BUTTONS ------------------------------------- */ .btn { box-sizing: border-box; width: 100%; } .btn > tbody > tr > td { padding-bottom: 15px; } .btn table { width: auto; } .btn table td { background-color: #ffffff; border-radius: 5px; text-align: center; } .btn a { background-color: #ffffff; border: solid 1px #3498db; border-radius: 5px; box-sizing: border-box; color: #3498db; cursor: pointer; display: inline-block; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-decoration: none; text-transform: capitalize; } .btn-primary table td { background-color: #3498db; } .btn-primary a { background-color: #3498db; border-color: #3498db; color: #ffffff; } /* ------------------------------------- OTHER STYLES THAT MIGHT BE USEFUL ------------------------------------- */ .last { margin-bottom: 0; } .first { margin-top: 0; } .align-center { text-align: center; } .align-right { text-align: right; } .align-left { text-align: left; } .clear { clear: both; } .mt0 { margin-top: 0; } .mb0 { margin-bottom: 0; } .preheader { color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0; } .powered-by a { text-decoration: none; } hr { border: 0; border-bottom: 1px solid #f6f6f6; Margin: 20px 0; } /* ------------------------------------- RESPONSIVE AND MOBILE FRIENDLY STYLES ------------------------------------- */ @media only screen and (max-width: 620px) { table[class=body] h1 { font-size: 28px !important; margin-bottom: 10px !important; } table[class=body] p, table[class=body] ul, table[class=body] ol, table[class=body] td, table[class=body] span, table[class=body] a { font-size: 16px !important; } table[class=body] .wrapper, table[class=body] .article { padding: 10px !important; } table[class=body] .content { padding: 0 !important; } table[class=body] .container { padding: 0 !important; width: 100% !important; } table[class=body] .main { border-left-width: 0 !important; border-radius: 0 !important; border-right-width: 0 !important; } table[class=body] .btn table { width: 100% !important; } table[class=body] .btn a { width: 100% !important; } table[class=body] .img-responsive { height: auto !important; max-width: 100% !important; width: auto !important; }} /* ------------------------------------- PRESERVE THESE STYLES IN THE HEAD ------------------------------------- */ @media all { .ExternalClass { width: 100%; } .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; } .apple-link a { color: inherit !important; font-family: inherit !important; font-size: inherit !important; font-weight: inherit !important; line-height: inherit !important; text-decoration: none !important; } .btn-primary table td:hover { background-color: #34495e !important; } .btn-primary a:hover { background-color: #34495e !important; border-color: #34495e !important; } } </style> </head> <body class=""> <table border="0" cellpadding="0" cellspacing="0" class="body"> <tr> <td>&nbsp;</td> <td class="container"> <div class="content"> <!-- START CENTERED WHITE CONTAINER --> <span class="preheader">This is preheader text. Some clients will show this text as a preview.</span> <table class="main"> <!-- START MAIN CONTENT AREA --> <tr> <td class="wrapper"> <table border="0" cellpadding="0" cellspacing="0"> <tr> <td> <p>Hi there,</p> <p>Sometimes you just want to send a simple HTML email with a simple design and clear call to action. This is it.</p> <table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary"> <tbody> <tr> <td align="left"> <table border="0" cellpadding="0" cellspacing="0"> <tbody> <tr> <td> <a href="http://htmlemail.io" target="_blank">Call To Action</a> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> <p>This is a really simple email template. Its sole purpose is to get the recipient to click the button with no distractions.</p> <p>Good luck! Hope it works.</p> </td> </tr> </table> </td> </tr> <!-- END MAIN CONTENT AREA --> </table> <!-- START FOOTER --> <div class="footer"> <table border="0" cellpadding="0" cellspacing="0"> <tr> <td class="content-block"> <span class="apple-link">Company Inc, 3 Abbey Road, San Francisco CA 94102</span> <br> Dont like these emails? <a href="http://i.imgur.com/CScmqnj.gif">Unsubscribe</a>. </td> </tr> <tr> <td class="content-block powered-by"> Powered by <a href="http://htmlemail.io">HTMLemail</a>. </td> </tr> </table> </div> <!-- END FOOTER --> <!-- END CENTERED WHITE CONTAINER --> </div> </td> <td>&nbsp;</td> </tr> </table> </body></html>' // html body
            html: req.body.html
        }; 
        transporter.sendMail(mailOptions, function(error, info){
        //smtpTransport.sendMail(mailOptions, function(error, info){
            if(error){
                return res.send(error);
            }
            return res.send("mail send successfully");
        }); 
    })

    app.post('/clave', function (req, res){
        
        var fechanumeros = req.body.fecha_contrato.replace("/", "");
        fechanumeros = fechanumeros.replace("/20", "");
       
        var clavereservacion = "";
        for (var i = 0, len = fechanumeros.length; i < len; i++) {
            if(i % 2){
                clavereservacion += (String.fromCharCode(97 + i).toUpperCase());
            }else{
                clavereservacion += fechanumeros[i];
            }
        }
        
        res.send(clavereservacion + req.body.destino_itinerario.charAt(0));
    })


    app.get('/contratos', function (req, res){
        
        var files = fs.readdirSync('./contratos');
        res.json(files);
    })

    app.post('/guardarcontrato', function(req, res){  

        var datos = req.body.fecha_contrato; 

        var writer = fs.createWriteStream("./contratos/"+req.body.nombre_contrato);
        response = {
            clave_reservacion: req.body.clave_reservacion,
            fecha_contrato: req.body.fecha_contrato,
            nombre_contratante: req.body.nombre_contratante,
            telefono_contratante: req.body.telefono_contratante,
            direccion_contratante: req.body.direccion_contratante,
            cliente_itinerario: req.body.cliente_itinerario,
            telefono_itinerario: req.body.telefono_itinerario,
            destino_itinerario: req.body.destino_itinerario,
	    ubicacion_destino_itinerario: req.body.ubicacion_destino_itinerario,
            fechasalida_itineario: req.body.fechasalida_itineario,
            presentarse_itineario: req.body.presentarse_itineario,
            horasalida_itineario: req.body.horasalida_itineario,
            direccionsalida_itinerario: req.body.direccionsalida_itinerario,
	    ubicacion_direccion_salida_itinerario: req.body.ubicacion_direccion_salida_itinerario,
            colonia_itineario: req.body.colonia_itineario,
            ciudad_itineario: req.body.ciudad_itineario,
            entrecalles_itinerario: req.body.entrecalles_itinerario,
            referencias_itinerario: req.body.referencias_itinerario,
            detalles_itineario: req.body.detalles_itineario,
            fecharegreso_itinerario: req.body.fecharegreso_itinerario,
            horaregreso_itineario: req.body.horaregreso_itineario,
            unidad_unidad: req.body.unidad_unidad,
            capacidad_unidad: req.body.capacidad_unidad,
            ACC_unidad: req.body.ACC_unidad,
            estereo_unidad: req.body.estereo_unidad,
            sanitarios_unidad: req.body.sanitarios_unidad,
            tvdvd_unidad: req.body.tvdvd_unidad,
            microfono_unidad: req.body.microfono_unidad,
            seguro_unidad: req.body.seguro_unidad,
            otros_unidad: req.body.otros_unidad,
            total_pagos: req.body.total_pagos,
            anticipo_pagos: req.body.anticipo_pagos,
            pendiente_pagos: req.body.pendiente_pagos
        }
        writer.write(JSON.stringify(response));

        return res.send(response);
	

    })

    app.post('/leercontrato', function (req, res){
        
        var content;
        // First I want to read the file
        fs.readFile('./contratos/'+req.body.nombre_contrato, "utf8", function read(err, data) {
            if (err) {
                throw err;
            }
            content = data;
            //console.log(content);   // Put all of the code here (not the best solution)
            res.send(content.toString());
        });


        
    })


    /**
     * RUTA CUPRUM SERVICES
     * 
     */

     app.post('/rutacuprum', function (req, res){
        
        var datos = req.body.info; 
	console.log("rutacuprum");
        //var writer = fs.createWriteStream("./ruta/datosruta.txt");
        fs.appendFileSync('./ruta/datosruta.csv', datos);
        //writer.write(datos);

        console.log(datos);
	return res.send(datos);


        
    })

   app.post('/rutacuprumgps', function (req, res){

       var content;
        // First I want to read the file
        fs.readFile('./ruta/datosruta.csv', "utf8", function read(err, data) {
            if (err) {
                throw err;
            }
            content = data;
            //console.log(content);   // Put all of the code here (not the best solution)

	    var lines = content.split("\n");
	    var result = "";
    
    	     for(var i=0; i<lines.length; i++)
       		 result = lines[i] + "\n" + result;


            res.send(result);
        });


    })

    app.post('/facturacuprum', function(req, res){  
        
        console.log("Procesado datos para enviar email....")
        // setup e-mail data with unicode symbols
        console.log(req.body.html);
        var mailOptions = {
            from: ''+config.mailUser+'', // sender address
            to: req.body.receivers, // list of receivers
            subject: req.body.subject + " - " + req.body.semana, // Subject line
            //text: req.body.text, // plaintext body
            //html: '<title>FACTURACION</title><header></header><style>.invoice-title h2,.invoice-title h3{display:inline-block}.table>tbody>tr>.no-line{border-top:none}.table>thead>tr>.no-line{border-bottom:none}.table>tbody>tr>.thick-line{border-top:2px solid}</style><div class=container><div class=row><div class=col-xs-12><div class=invoice-title><h2>RUTA CUPRUM GUAGUA EMPRESARIAL</h2><br><strong>NumeroServicio # '+req.body.numeroservicio+'</strong></div><hr><div class=row><div class=col-xs-12><address><strong>Semana:</strong> '+req.body.semana+'<br><strong>Servicio de:</strong> CUPRUM <strong>Operado por:</strong> Manuel Gonzalez<br></address></div></div><div class=row><div class=col-xs-12><address><strong>Precio por ruta: </strong>$179.50<br><strong>Precio por servicio (ida y regreso)</strong> $359 <strong>Precio final con iva:</strong> $416.44<br></address></div></div></div><div class=row><div class=col-md-12><div class="panel panel-default"><div class=panel-heading><h3 class=panel-title><strong>Servicios realizados</strong></h3></div><div class=panel-body><div class=table-responsive><table class="table table-condensed"><thead><tr><td><strong>Fecha NNNN-MM-DD</strong><td class=text-center><strong>Numero de Servicios</strong><td class=text-center><strong>Precio servicio</strong><td class=text-right><strong>Precio total</strong><tbody><tr><td>'+req.body.dia1.fecha+'<td class=text-center>'+req.body.dia1.Nservicios+'<td class=text-center>'+req.body.dia1.PrecioUnitario+'<td class=text-right>'+req.body.dia1.Precio+'<tr><td>'+req.body.dia2.fecha+'<td class=text-center>'+req.body.dia2.Nservicios+'<td class=text-center>'+req.body.dia2.PrecioUnitario+'<td class=text-right>'+req.body.dia2.Precio+'<tr><td>'+req.body.dia3.fecha+'<td class=text-center>'+req.body.dia3.Nservicios+'<td class=text-center>'+req.body.dia3.PrecioUnitario+'<td class=text-right>'+req.body.dia3.Precio+'<tr><td>'+req.body.dia4.fecha+'<td class=text-center>'+req.body.dia4.Nservicios+'<td class=text-center>'+req.body.dia4.PrecioUnitario+'<td class=text-right>'+req.body.dia4.Precio+'<tr><td>'+req.body.dia5.fecha+'<td class=text-center>'+req.body.dia5.Nservicios+'<td class=text-center>'+req.body.dia5.PrecioUnitario+'<td class=text-right>'+req.body.dia5.Precio+'<tr><td>'+req.body.dia6.fecha+'<td class=text-center>'+req.body.dia6.Nservicios+'<td class=text-center>'+req.body.dia6.PrecioUnitario+'<td class=text-right>'+req.body.dia6.Precio+'<tr><td class=thick-line><td class="text-center thick-line"><td class="text-center thick-line"><strong>SUB total</strong><td class="text-right thick-line">'+req.body.total.subtotal+'<tr><td class=no-line><td class=no-line><td class="text-center no-line"><strong>IVA</strong><td class="text-right no-line">'+req.body.total.iva+'<tr><td class=no-line><td class=no-line><td class="text-center no-line"><strong>TOTAL</strong><td class="text-right no-line">'+req.body.total.total+'</table></div></div></div></div></div>' // html body
            html: '<!DOCTYPE html><title>FACTURACION</title><style>.invoice-title h2,.invoice-title h3{display:inline-block}.table>tbody>tr>.no-line{border-top:none}.table>thead>tr>.no-line{border-bottom:none}.table>tbody>tr>.thick-line{border-top:2px solid}</style><style>body,html{margin:0 auto!important;padding:0!important;height:100%!important;width:100%!important}*{-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}div[style*="margin: 16px 0"]{margin:0!important}table,td{mso-table-lspace:0!important;mso-table-rspace:0!important}table{border-spacing:0!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0 auto!important}table table table{table-layout:auto}img{-ms-interpolation-mode:bicubic}.aBn,.x-gmail-data-detectors,.x-gmail-data-detectors *,[x-apple-data-detectors]{border-bottom:0!important;cursor:default!important;color:inherit!important;text-decoration:none!important;font-size:inherit!important;font-family:inherit!important;font-weight:inherit!important;line-height:inherit!important}.a6S{display:none!important;opacity:.01!important}img.g-img+div{display:none!important}.button-link{text-decoration:none!important}@media only screen and (min-device-width:375px) and (max-device-width:413px){.email-container{min-width:375px!important}}</style><style>.button-a,.button-td{transition:all .1s ease-in}.button-a:hover,.button-td:hover{background:#555!important;border-color:#555!important}@media screen and (max-width:600px){.email-container p{font-size:17px!important;line-height:22px!important}}</style><body bgcolor=#FFFFFF style=margin:0;mso-line-height-rule:exactly><center style=width:100%;background:#fff;text-align:left><div class=email-container style=max-width:600px;margin:auto><div class=row><div class=col-xs-12><div class=invoice-title><h2>RUTA CUPRUM GUAGUA EMPRESARIAL</h2><br><strong>NumeroServicio # '+req.body.numeroservicio+'</strong></div><hr><div class=row><div class=col-xs-12><address><strong>Semana:</strong> '+req.body.semana+'<br><strong>Servicio de:</strong> CUPRUM <strong>Operado por:</strong> Manuel Gonzalez<br></address></div></div><div class=row><div class=col-xs-12><address><strong>Precio por ruta:</strong> $179.50<br><strong>Precio por servicio (ida y regreso)</strong> $359 <strong>Precio final con iva:</strong> $416.44<br></address></div></div></div><div class=row><div class=col-md-12><div class="panel panel-default"><div class=panel-heading><h3 class=panel-title><strong>Servicios realizados</strong></h3></div><div class=panel-body><div class=table-responsive><table class="table table-condensed"><thead><tr><td><strong>Fecha NNNN-MM-DD</strong><td class=text-center><strong>Numero de Servicios</strong><td class=text-center><strong>Precio servicio</strong><td class=text-right><strong>Precio total</strong><tbody><tr><td>'+req.body.dia1.fecha+'<td class=text-center>'+req.body.dia1.Nservicios+'<td class=text-center>'+req.body.dia1.PrecioUnitario+'<td class=text-right>'+req.body.dia1.Precio+'<tr><td>'+req.body.dia2.fecha+'<td class=text-center>'+req.body.dia2.Nservicios+'<td class=text-center>'+req.body.dia2.PrecioUnitario+'<td class=text-right>'+req.body.dia2.Precio+'<tr><td>'+req.body.dia3.fecha+'<td class=text-center>'+req.body.dia3.Nservicios+'<td class=text-center>'+req.body.dia3.PrecioUnitario+'<td class=text-right>'+req.body.dia3.Precio+'<tr><td>'+req.body.dia4.fecha+'<td class=text-center>'+req.body.dia4.Nservicios+'<td class=text-center>'+req.body.dia4.PrecioUnitario+'<td class=text-right>'+req.body.dia4.Precio+'<tr><td>'+req.body.dia5.fecha+'<td class=text-center>'+req.body.dia5.Nservicios+'<td class=text-center>'+req.body.dia5.PrecioUnitario+'<td class=text-right>'+req.body.dia5.Precio+'<tr><td>'+req.body.dia6.fecha+'<td class=text-center>'+req.body.dia6.Nservicios+'<td class=text-center>'+req.body.dia6.PrecioUnitario+'<td class=text-right>'+req.body.dia6.Precio+'<tr><td class=thick-line><td class="text-center thick-line"><td class="text-center thick-line"><strong>SUB total</strong><td class="text-right thick-line">'+req.body.total.subtotal+'<tr><td class=no-line><td class=no-line><td class="text-center no-line"><strong>IVA</strong><td class="text-right no-line">'+req.body.total.iva+'<tr><td class=no-line><td class=no-line><td class="text-center no-line"><strong>TOTAL</strong><td class="text-right no-line">'+req.body.total.total+'</table></div></div></div></div></div></div></div></center>'
            //html: req.body.html
        }; 
        transporter.sendMail(mailOptions, function(error, info){
        //smtpTransport.sendMail(mailOptions, function(error, info){
            if(error){
                return res.send(error);
            }
            return res.send("mail send successfully");
        }); 
    })

    app.post('/facturacuprumv2', function(req, res){  
        
        console.log("Procesado datos para enviar email....")
        // setup e-mail data with unicode symbols
        console.log(req.body.html);
        var mailOptions = {
            from: ''+config.mailUser+'', // sender address
            to: req.body.receivers, // list of receivers
            subject: req.body.subject + " - " + req.body.semana, // Subject line
            //text: req.body.text, // plaintext body
            //html: '<title>FACTURACION</title><header></header><style>.invoice-title h2,.invoice-title h3{display:inline-block}.table>tbody>tr>.no-line{border-top:none}.table>thead>tr>.no-line{border-bottom:none}.table>tbody>tr>.thick-line{border-top:2px solid}</style><div class=container><div class=row><div class=col-xs-12><div class=invoice-title><h2>RUTA CUPRUM GUAGUA EMPRESARIAL</h2><br><strong>NumeroServicio # '+req.body.numeroservicio+'</strong></div><hr><div class=row><div class=col-xs-12><address><strong>Semana:</strong> '+req.body.semana+'<br><strong>Servicio de:</strong> CUPRUM <strong>Operado por:</strong> Manuel Gonzalez<br></address></div></div><div class=row><div class=col-xs-12><address><strong>Precio por ruta: </strong>$179.50<br><strong>Precio por servicio (ida y regreso)</strong> $359 <strong>Precio final con iva:</strong> $416.44<br></address></div></div></div><div class=row><div class=col-md-12><div class="panel panel-default"><div class=panel-heading><h3 class=panel-title><strong>Servicios realizados</strong></h3></div><div class=panel-body><div class=table-responsive><table class="table table-condensed"><thead><tr><td><strong>Fecha NNNN-MM-DD</strong><td class=text-center><strong>Numero de Servicios</strong><td class=text-center><strong>Precio servicio</strong><td class=text-right><strong>Precio total</strong><tbody><tr><td>'+req.body.dia1.fecha+'<td class=text-center>'+req.body.dia1.Nservicios+'<td class=text-center>'+req.body.dia1.PrecioUnitario+'<td class=text-right>'+req.body.dia1.Precio+'<tr><td>'+req.body.dia2.fecha+'<td class=text-center>'+req.body.dia2.Nservicios+'<td class=text-center>'+req.body.dia2.PrecioUnitario+'<td class=text-right>'+req.body.dia2.Precio+'<tr><td>'+req.body.dia3.fecha+'<td class=text-center>'+req.body.dia3.Nservicios+'<td class=text-center>'+req.body.dia3.PrecioUnitario+'<td class=text-right>'+req.body.dia3.Precio+'<tr><td>'+req.body.dia4.fecha+'<td class=text-center>'+req.body.dia4.Nservicios+'<td class=text-center>'+req.body.dia4.PrecioUnitario+'<td class=text-right>'+req.body.dia4.Precio+'<tr><td>'+req.body.dia5.fecha+'<td class=text-center>'+req.body.dia5.Nservicios+'<td class=text-center>'+req.body.dia5.PrecioUnitario+'<td class=text-right>'+req.body.dia5.Precio+'<tr><td>'+req.body.dia6.fecha+'<td class=text-center>'+req.body.dia6.Nservicios+'<td class=text-center>'+req.body.dia6.PrecioUnitario+'<td class=text-right>'+req.body.dia6.Precio+'<tr><td class=thick-line><td class="text-center thick-line"><td class="text-center thick-line"><strong>SUB total</strong><td class="text-right thick-line">'+req.body.total.subtotal+'<tr><td class=no-line><td class=no-line><td class="text-center no-line"><strong>IVA</strong><td class="text-right no-line">'+req.body.total.iva+'<tr><td class=no-line><td class=no-line><td class="text-center no-line"><strong>TOTAL</strong><td class="text-right no-line">'+req.body.total.total+'</table></div></div></div></div></div>' // html body
            html: '<!DOCTYPE html><html xmlns=http://www.w3.org/1999/xhtml><meta content="width=device-width"name=viewport><meta content="text/html; charset=UTF-8"http-equiv=Content-Type><title>Invoices or receipts</title><style>*{margin:0;padding:0;font-family:"Helvetica Neue",Helvetica,Helvetica,Arial,sans-serif;box-sizing:border-box;font-size:14px}img{max-width:100%}body{-webkit-font-smoothing:antialiased;-webkit-text-size-adjust:none;width:100%!important;height:100%;line-height:1.6}table td{vertical-align:top}body{background-color:#f6f6f6}.body-wrap{background-color:#f6f6f6;width:100%}.container{display:block!important;max-width:600px!important;margin:0 auto!important;clear:both!important}.content{max-width:600px;margin:0 auto;display:block;padding:20px}.main{background:#fff;border:1px solid #e9e9e9;border-radius:3px}.content-wrap{padding:20px}.content-block{padding:0 0 20px}.header{width:100%;margin-bottom:20px}.footer{width:100%;clear:both;color:#999;padding:20px}.footer a{color:#999}.footer a,.footer p,.footer td,.footer unsubscribe{font-size:12px}.column-left{float:left;width:50%}.column-right{float:left;width:50%}h1,h2,h3{font-family:"Helvetica Neue",Helvetica,Arial,"Lucida Grande",sans-serif;color:#000;margin:40px 0 0;line-height:1.2;font-weight:400}h1{font-size:32px;font-weight:500}h2{font-size:24px}h3{font-size:18px}h4{font-size:14px;font-weight:600}ol,p,ul{margin-bottom:10px;font-weight:400}ol li,p li,ul li{margin-left:5px;list-style-position:inside}a{color:#348eda;text-decoration:underline}.btn-primary{text-decoration:none;color:#fff;background-color:#348eda;border:solid #348eda;border-width:10px 20px;line-height:2;font-weight:700;text-align:center;cursor:pointer;display:inline-block;border-radius:5px;text-transform:capitalize}.last{margin-bottom:0}.first{margin-top:0}.padding{padding:10px 0}.aligncenter{text-align:center}.alignright{text-align:right}.alignleft{text-align:left}.clear{clear:both}.alert{font-size:16px;color:#fff;font-weight:500;padding:20px;text-align:center;border-radius:3px 3px 0 0}.alert a{color:#fff;text-decoration:none;font-weight:500;font-size:16px}.alert.alert-warning{background:#ff9f00}.alert.alert-bad{background:#d0021b}.alert.alert-good{background:#68b90f}.invoice{margin:40px auto;text-align:left;width:80%}.invoice td{padding:5px 0}.invoice .invoice-items{width:100%}.invoice .invoice-items td{border-top:#eee 1px solid}.invoice .invoice-items .total td{border-top:2px solid #333;border-bottom:2px solid #333;font-weight:700}@media only screen and (max-width:640px){h1,h2,h3,h4{font-weight:600!important;margin:20px 0 5px!important}h1{font-size:22px!important}h2{font-size:18px!important}h3{font-size:16px!important}.container{width:100%!important}.content,.content-wrapper{padding:10px!important}.invoice{width:100%!important}}</style><table class=body-wrap><tr><td><td class=container width=600><div class=content><table class=main cellpadding=0 cellspacing=0 width=100%><tr><td class="aligncenter content-wrap"><table cellpadding=0 cellspacing=0 width=100%><tr><td class=content-block><h1>$33.98 Paid</h1><tr><td class=content-block><h2>Thanks for using Acme Inc.</h2><tr><td class=content-block><table class=invoice><tr><td>Lee Munroe<br>Invoice #12345<br>June 01 2014<tr><td><table class=invoice-items cellpadding=0 cellspacing=0><tr><td>Service 1<td class=alignright>$ 19.99<tr><td>Service 2<td class=alignright>$ 9.99<tr><td>Service 3<td class=alignright>$ 4.00<tr class=total><td class=alignright width=80%>Total<td class=alignright>$ 33.98</table></table><tr><td class=content-block><a href=http://www.mailgun.com>View in browser</a><tr><td class=content-block>Acme Inc. 123 Van Ness, San Francisco 94102</table></table><div class=footer><table width=100%><tr><td class="content-block aligncenter">Questions? Email <a href=mailto:>support@acme.inc</a></table></div></div><td></table>'
            //html: req.body.html
        }; 
        transporter.sendMail(mailOptions, function(error, info){
        //smtpTransport.sendMail(mailOptions, function(error, info){
            if(error){
                return res.send(error);
            }
            return res.send("mail send successfully");
        }); 
    })

    

};
